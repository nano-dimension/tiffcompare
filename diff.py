# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
import sys
import os
from collections import namedtuple
from enum import Enum
from os import listdir
from os.path import isfile, join, splitext, basename
from typing import List, Union, Deque, Tuple, Optional
import cv2
import numpy as np
from scipy import signal
import time
from zipfile import ZipFile

args_type = namedtuple("args_type", ["dir1", "dir2", "threshold"])
args_dict = {k: "" for k in args_type._fields}
times_dict = {}
pcbjc_mode = False

def times(key: str, time_elapsed: Optional[float] = None):
    if key not in times_dict:
        times_dict[key] = []
    if time_elapsed is None:
        return times_dict[key]
    times_dict[key].append(time_elapsed)


def get_arg(arg_name):
    prefix = f'-{arg_name}:'
    fit_args = set(map(lambda x: x.replace(prefix, "").replace('"', ""), filter(lambda x: x.startswith(prefix), sys.argv)))
    if len(fit_args) == 0:
        print(f"required parameter '{arg_name}' is missing.\nPlease insert is using '{prefix}\"value\"'.")
        exit(0)
    if len(fit_args) > 1:
        print(f"used {arg_name} {len(fit_args)} times.\nPlease use it once.")
        exit(0)
    args_dict[arg_name] = list(fit_args)[0]
    return list(fit_args)[0]


for my_arg_name in args_type._fields:
    get_arg(my_arg_name)

args_dict["threshold"] = int(args_dict["threshold"])
args = args_type(**args_dict)


class DIRECTION(Enum):
    value: int
    DOWN = 0
    RIGHT = 1


Difference = namedtuple("Difference", ["array", "size"])


class row_in_csv:
    def __init__(self, file="", overall=-1, blues=-1):
        self.file = basename(file)
        self.overall = str(overall)
        self.red = str(overall - blues)
        self.blues = str(blues)

    def __repr__(self):
        return ','.join(list(self.__dict__.values()))


out_csv_lst = [','.join(list(vars(row_in_csv()).keys()))]


def cut_img(image, shape: Tuple[int, int]) -> np.ndarray:
    rows, cols = min(shape[0], image.shape[0]), min(shape[1], image.shape[1])
    return image[:rows, :cols]
    # returned_image = image[:rows, :cols, :]
    # while np.shape(returned_image)[0] < rows:
    #     returned_image = np.concatenate([np.zeros((1, np.shape(returned_image)[1], 3)), returned_image])
    # while np.shape(returned_image)[1] < cols:
    #     returned_image = np.column_stack([np.zeros((np.shape(returned_image)[0], 1, 3)), returned_image])
    # return returned_image, returned_image[:rows, :cols, 1]


class COLORS:
    BLUE = [255, 0, 0]
    RED = [0, 0, 255]


def color_the_image(image3: np.ndarray, diffs: Difference):
    threshold_shape = np.ones((args.threshold, args.threshold))
    threshold_real = int(np.sum(threshold_shape))
    conv_result = my_convolution(diffs.array, threshold_shape) >= round(threshold_real)
    # containing_square = zip(*np.where())
    # for r, c in containing_square:
    #     spread_color(image3, diffs, color, r, c)
    image3[diffs.array] = COLORS.BLUE
    image3[conv_result] = COLORS.RED


def my_convolution(diffs, threshold_shape):
    conv = signal.convolve2d(diffs, threshold_shape)
    shp_conv = conv.shape
    shp_diff = diffs.shape
    dif_h = shp_conv[0] - shp_diff[0]
    dif_w = shp_conv[1] - shp_diff[1]
    slice_bot = dif_h // 2
    slice_top = dif_h // 2 - dif_h
    slice_lef = dif_w // 2
    slice_rig = dif_w // 2 - dif_w
    return conv[slice_bot:slice_top, slice_lef:slice_rig]


def post(image1, filename, diff, who_right=None, right=None, who_down=None, down=None):
    postfix = '_org'
    if right is not None:
        sign_r = (2 * who_right - 3)
        sign_d = (2 * who_down - 3)
        postfix = f'opt_{sign_r * right}_{sign_d * down}'
    image3 = image1.copy()
    color_the_image(image3, diff)

    output_path = join(os.getcwd(), 'output\\' + basename(filename).replace(".tif", postfix + ".tif"))
    post_message("output: " + output_path)
    cv2.imwrite(output_path, image3)
    blues = int(np.sum(np.all(image3 == [255, 0, 0], axis=2)))
    all_e = int(np.sum(diff.array))
    out_csv_lst.append(str(row_in_csv(output_path, all_e, blues)))
    print("[CRT]:", output_path)


def read_file(filename1):
    with open(filename1, 'r') as f:
        return f.read()


def compare(filename1: str, filename2: str):
    if pcbjc_mode:
        if os.path.basename(filename1) == "pcbj.info":
            assert os.path.basename(filename1) == os.path.basename(filename2)
            if read_file(filename1) == read_file(filename2):
                return
            else:
                exit(-1)
    image1 = np.array(cv2.imread(filename1)).astype(np.int)
    image2 = np.array(cv2.imread(filename2)).astype(np.int)
    if not pcbjc_mode:
        image1 = cut_img(image1, image2.shape)
        image2 = cut_img(image2, image1.shape)
    diff_org = get_diff(image1, image2)
    post(image1, filename1, diff_org)

    if not pcbjc_mode:
        diff_opt, image1_opt, image2_opt, moved_right, r_off, moved_down, d_off = find_opt_diff(image1, image2, diff_org)
        post(image1, filename1, diff_opt, moved_right, r_off, moved_down, d_off)

    return


def find_opt_diff(image1: np.ndarray, image2: np.ndarray, diff_org: Difference):
    r_diff_1, r_off_1, r_img_1 = find_opt_diff_by_direction(image1, image2, diff_org, DIRECTION.RIGHT)
    r_diff_2, r_off_2, r_img_2 = find_opt_diff_by_direction(image2, image1, diff_org, DIRECTION.RIGHT)

    moved_right, r_diff, r_off, image1, image2 = (1, r_diff_1, r_off_1, r_img_1, image2) if r_diff_1.size <= r_diff_2.size else (2, r_diff_2, r_off_2, image1, r_img_2)

    d_diff_1, d_off_1, d_img_1 = find_opt_diff_by_direction(image1, image2, r_diff, DIRECTION.DOWN)
    d_diff_2, d_off_2, d_img_2 = find_opt_diff_by_direction(image2, image1, r_diff, DIRECTION.DOWN)
    moved_down, d_diff, d_off, image1, image2 = (1, d_diff_1, d_off_1, d_img_1, image2) if d_diff_1.size <= d_diff_2.size else (2, d_diff_2, d_off_2, image1, d_img_2)
    assert d_diff.size <= r_diff.size
    return d_diff, image1, image2, moved_right, r_off, moved_down, d_off


def find_opt_diff_by_direction(image1: np.ndarray, image2: np.ndarray, diff_old: Difference, direction: DIRECTION) -> Tuple[Difference, int, np.ndarray]:
    image1_after, diff_after = image1, diff_old
    offset_right = 0
    while diff_after.size <= diff_old.size and diff_old.size != 0:
        image1, diff_old = image1_after, diff_after
        offset_right += 1
        image1_after = step(image1, direction)
        diff_after = get_diff(image1_after, image2)

    return diff_old, offset_right - 1, image1


def get_diff(image1: np.ndarray, image2: np.ndarray) -> Difference:
    image1 = cut_img(image1, image2.shape)
    image2 = cut_img(image2, image1.shape)
    not_same = (image1 != image2).all(axis=2)

    return Difference(not_same, np.sum(not_same))


def step(image: np.ndarray, direction: DIRECTION):
    return move(image, 1, direction)


def move(image: np.ndarray, offset: int, direction: DIRECTION):
    shape = list(image.shape)  # shallow copy the tuple shape
    shape[direction.value] = offset
    shape = tuple(shape)
    columns = np.zeros(shape)

    result = np.concatenate((columns, image), axis=direction.value)
    return result


def move_right(image, offset):
    return move(image, offset, DIRECTION.RIGHT)


def move_down(image, offset):
    return move(image, offset, DIRECTION.DOWN)


def get_files_names_by_path(path_folder) -> List[str]:
    if os.path.isdir(path_folder):
        return sorted([join(path_folder, f) for f in listdir(path_folder) if isfile(join(path_folder, f))])
    elif isfile(path_folder):
        global pcbjc_mode
        pcbjc_mode = True
        with ZipFile(path_folder, "r") as zf:
            zf.extractall("temp/"+basename(path_folder))
            return get_files_names_by_path("temp/"+basename(path_folder))


def post_message(msgs: Union[List[str], str], spaces=1, prefix='MSG', dot='.'):
    if prefix != "":
        prefix = '[' + prefix + ']'  # ex: '[MSG] ', '[ERROR] ',...
    while spaces > 0:
        prefix += ' '
        spaces -= 1
    if isinstance(msgs, str):
        msgs = [msgs]
    for msg in msgs:
        print(prefix, msg, dot, sep='')


def post_error(errs: Union[List[str], str], dot='.'):
    post_message(errs, prefix='ERROR', dot=dot)
    exit(0)


def compare_for_yoni(filename1, filename2):

    return compare(filename1, filename2)


def act():
    if len(sys.argv) < 3:
        print("Usage: diff.py [path to folder 1] [path to folder 2]")
    path1 = args.dir1
    path2 = args.dir2
    files1 = get_files_names_by_path(path1)
    files2 = get_files_names_by_path(path2)
    if len(files1) != len(files2):
        post_error([
            f"In folder 1 ('{path1}') there are {len(files1)} files",
            f"In folder 2 ('{path2}') there are {len(files2)} files"
        ])
    for i, (filename1, filename2) in enumerate(zip(files1, files2)):
        # no_ext1 = splitext(basename(filename1))[0]
        # no_ext2 = splitext(basename(filename2))[0]
        # if no_ext1 != no_ext2:
        #     post_error([
        #         f"Mismatched files in folder 1 ('{path1}') and in folder 2 ('{path2}')",
        #         f"{splitext(filename1)[0]} != {splitext(filename2)[0]}"
        #     ])

        post_message(filename1, spaces=2, prefix="START")

        if pcbjc_mode:
            compare_for_yoni(filename1, filename2)
        else:
            compare(filename1, filename2)
        post_message(filename1, prefix="FINISH "+str(i)+"/"+str(len(files1)))


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    out_dir = join(os.getcwd(), 'output')
    if os.path.isdir(out_dir):
        for i in os.listdir(out_dir):
            os.remove(join(out_dir, i))
    else:
        os.makedirs(out_dir)
    ts = time.time()
    act()
    te = time.time()
    spread_times = np.sort(times('spread_color'))
    if len(spread_times)>0:
        avg = np.average(spread_times)
        std = np.std(spread_times)

        print()
        print("times:", spread_times)
        print("parted:")
        batch_size = 40000
        for i in range(len(spread_times) // 40000):
            print(np.average(spread_times[batch_size * i:batch_size * (i + 1)]))
        print(np.average(spread_times[(len(spread_times) // 40000) * batch_size:]))
        print("std:", std)
        print("sum:", np.sum(spread_times))
        print("avg:", avg)
        print("max:", np.max(spread_times))
        print("count:", len(spread_times))
        print("bigger:", np.sum(spread_times > avg), '~', np.sum(spread_times > avg) * 100 // len(spread_times))
        print("bigger*10:", np.sum(spread_times > 10 * avg), '~', np.sum(spread_times > 10 * avg) * 100 // len(spread_times))

    print("real_sum:", te - ts)

    csv_txt = '\n'.join(map(str, out_csv_lst))

    with open(join(os.getcwd(), 'output\\_report.csv'), "w+") as csv:
        csv.write(csv_txt)
